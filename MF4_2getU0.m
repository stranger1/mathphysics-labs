function z = MF4_2getU0( x, type )

    switch type
        
        case 1
            z = 5*x + 1;
        case 2
            z = (x + 1)^2;   
        case 3
            z= exp(-x);
    end;


end

