function [ X] = thomas(A, B)

n = length(B);

P(1) = -A(1,3)/A(1,2);
Q(1) = B(1)/A(1,2);

for i=1:n-1
	P(i+1) = -A(i+1,3)/(A(i+1,2)+A(i+1,1)*P(i));
	Q(i+1) = (-A(i+1,1)*Q(i)+B(i+1))/(A(i+1,2)+A(i+1,1)*P(i));
end;
		
X(n) = 	Q(n);

for i=n-2:-1:0
	X(i+1) = P(i+1)*X(i+2)+Q(i+1);
end;


end

