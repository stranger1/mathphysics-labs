function solveMF1_2a( q_x, k, beta, gamma1, gamma2, f_x, l )
%Solve following boundary value problem:
%  k*u''(x)-q(x)*u(x) = -f(x), 0<x<l
%  -k*u'(0)+beta*u(0)=gamma1, u(l)=gamma2
%
%usage:
%   solveMF1_2a( q_x, k, beta, gamma1, gamma2, f_x, l )
    
    h = l/500;
    x = 0:h:l;

    N = length(x);

    D = zeros(1, N); %main diagonal of matrix
    D(1) = k/h + beta;
    for i = 2:N-1
        D(i) = -2*k/(h^2) - q_x(x(i));
    end;
    D(N) = 1;

    B = zeros(1, N); %diagonal below the main
    for i = 2:N
        B(i) = k/(h^2);
    end;
    B(N) = 0;

    A = zeros(1, N); %diagonal above the main
    A(1) = -k/h;
    for i = 2:N
        A(i) = k/(h^2);
    end;
    
    %Tridiagonal matrix algorithm (Metod Progonki)
    % vector of free members: (gamma1, f_x(x(2)), f_x(x(3)),...,f_x(x(N-1)), gamma2)
    f = zeros(1,N);
    f(1) = gamma1;
    for i = 2:N-1
        f(i) = -f_x(x(i));
    end;
    f(N) = gamma2;
   
    y = TDMAlg( B, D, A, f );

    xx = 0:0.1:l;
%    examples:
%    plot( x, y, xx, xx.^3);
%    plot( x, y, xx, (exp(-xx).*(-1+exp(2*xx-10).*(xx-5)-2*exp(2*xx).*(-5 + xx))) / 4);
%    plot( x, y, xx, cos(xx));

    plot( x, y );
end
