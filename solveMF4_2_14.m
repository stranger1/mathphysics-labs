function [X, Y, Z] = solveMF4_2_14( h, tau, L, T, exampleN)

    n = fix(L/h);
    m = fix(T/tau);
	
	for k=0:m
        Z(1,k+1) = MF4_2getG0(k*tau, exampleN);
		Z(n+1,k+1) = MF4_2getG1(k*tau, exampleN);

        Y(k+1) = k*tau;
    end;
	
	
	for k=0:n
        Z(k+1,1) = MF4_2getU0(k*h, exampleN);
        X(k+1) = k*h;
    end;
	
    for j=0:m-1

		mu = MF4_2getMu(h, (j+0.5)*tau, exampleN);
		v = MF4_2getV(h, (j+0.5)*tau, exampleN);
		f = 0;
		kf = MF4_2getK(h, (j+0.5)*tau, exampleN);

		A = zeros(n-1,3);
		B = zeros(n-1,1);
		
		A(1,1)=0;
		A(1,2)=(1/tau + mu/h/h +kf/2);
		A(1,3)=v/4/h - mu/2/h/h;
		B(1) = f - Z(1,j+1)*(-v/4/h-mu/2/h/h) - Z(2,j+1)*(-1/tau+mu/h/h+kf/2) - Z(3,j+1)*(v/4/h-mu/2/h/h)- Z(1,j+2)*(-v/4/h-mu/2/h/h);
		

			mu = MF4_2getMu(L-h, (j+0.5)*tau, exampleN);
			v = MF4_2getV(L-h, (j+0.5)*tau, exampleN);
			f = 0;
			kf = MF4_2getK(L-h, (j+0.5)*tau, exampleN);
		
		
		
		B(n-1) = f - Z(n-1,j+1)*(-v/4/h-mu/2/h/h) - Z(n,j+1)*(-1/tau+mu/h/h+kf/2) - Z(n+1,j+1)*(v/4/h-mu/2/h/h)- Z(n-1,j+2)*(-v/4/h-mu/2/h/h)- Z(n+1,j+2)*(v/4/h-mu/2/h/h);
		
		A(n-1,1)=-v/4/h - mu/2/h/h;
		A(n-1,2)=(1/tau + mu/h/h +kf/2);
		A(n-1,3)=0;
		
		for k=1:n-3
            
			mu = MF4_2getMu((k+1)*h, (j+0.5)*tau, exampleN);
			v = MF4_2getV((k+1)*h, (j+0.5)*tau, exampleN);
			f = 0;
			kf = MF4_2getK((k+1)*h, (j+0.5)*tau, exampleN);
            

				A(k+1,1) = -v/4/h - mu/2/h/h;
				A(k+1,2) = (1/tau + mu/h/h +kf/2);
				A(k+1,3) = v/4/h - mu/2/h/h;
			
				B(k+1) = f - Z(k+1,j+1)*(-v/4/h-mu/2/h/h) - Z(k+2,j+1)*(-1/tau+mu/h/h+kf/2) - Z(k+3,j+1)*(v/4/h-mu/2/h/h);
			
			 
        end;
		
		
		
		%disp('A' + A);
		%disp('B' + B);
		RES = thomas(A,B);

		
		for i=0:n-2
			
			Z(i+2,j+2) = RES(i+1);
		
		end;
		
        
    end;

 	figure(1);
	mesh(Y,X,Z);
	title("Approximate solution");
	figure(2);
	XR = 0:0.01:L;
	YR = 0:0.01:T;
	ZR = getRealFunc(XR, YR, exampleN);
	mesh(YR, XR, ZR);
	title("Accurate solution");

	function z = getRealFunc( x, t , exampleN)
		mu = MF4_2getMu(x, t, exampleN);
		z = zeros(length(x), length(t));
		for i = 1:length(x)
			for j = 1:length(t)
				switch exampleN   
					case 1
						z(i,j)=(5*x(i)+5*exp(t(j)) + 1)*exp(t(j));
					case 2
						z(i,j)=x(i)*x(i) + exp(2*t(j));
					case 3
						z(i,j)=exp(-x(i)+mu*t(j));
				end;
			end;
		end;

	end;   
    
    
end

%example: solveMF4_2_14( 0.1, 0.01, 1, 1, #)

