function [X, Y, Z] = solveMF4_2_4( h, tau, L, T, exampleN)

    n = fix(L/h)+1;
    m = fix(T/tau)+1;
    
    for k=1:n
        Z(k,1) = MF4_2getU0((k-1)*h, exampleN);
        X(k) = (k-1)*h;
    end;

    for k=2:m
        Z(1,k) = MF4_2getG0((k-1)*tau, exampleN);
		Z(n,k) = MF4_2getG1((k-1)*tau, exampleN);
		
        Y(k) = (k-1)*tau;
    end;
	
    for j=1:m-1
       for k=2:n-1
			mu = MF4_2getMu((k-1), (j-1), exampleN);
			v = MF4_2getV((k-1), (j-1), exampleN);
			f = 0;
			kf = MF4_2getK((k-1), (j-1), exampleN);

			Z(k,j+1)=tau*( (1/tau-2*mu/h/h-kf)*Z(k,j)+(-v/2/h + mu/h/h)*Z(k+1,j)+ (v/2/h + mu/h/h)*Z(k-1,j) + f);
			 
       end;
        
    end;
	figure(1);
	mesh(Y,X,Z);
	title("Approximate solution");
	figure(2);
	XR = 0:0.01:L;
	YR = 0:0.01:T;
	ZR = getRealFunc(XR, YR, exampleN);
	mesh(YR, XR, ZR);
	title("Accurate solution");

	function z = getRealFunc( x, t , exampleN)
		mu = MF4_2getMu(x, t, exampleN);
		z = zeros(length(x), length(t));
		for i = 1:length(x)
			for j = 1:length(t)
				switch exampleN   
					case 1
						z(i,j)=(5*x(i)+5*exp(t(j)) + 1)*exp(t(j));
					case 2
						z(i,j)=((x(i)+exp(t(j)))^2 + 2*t(j))* exp(t(j)^2);
					case 3
						z(i,j)=exp(-x(i)+mu*t(j));
				end;
			end;
		end;

	end;
end

%example: solveMF4_2_4( 0.1, 0.01, 1, 1, #)

%stable: solveMF4_2_4 (0.1, 0.01, 1, 1,3)

%unstable: solveMF4_2_4(0.1, 0.01, 1, 1,1)

