function z = MF4_2getG0( t, type )
	mu = MF4_2getMu(1, t, type);
    switch type
        
        case 1
            z=(5*exp(t) + 1) * exp(t^2);
        case 2
            z=exp(t^2) * (exp(2*t) + 2*t); 
        case 3
            z=exp(mu*t);
    end;


end

