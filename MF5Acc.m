function z = MF5Acc(exampleN, x, y, t)

	mu = MF5_Mu(x,y,t, exampleN);
	
	switch exampleN
	
		case 1
			z = 5 * cos(2*x + 1) * cos(y) * exp(-5*mu*t);
		case 2
			z = 4 * exp(x + y + 2*mu*t);
		case 3
			z = 5 * cos(x - 1) * cos(y + 1) * exp(-2*mu*t);
	end;


end

%MF5_PrintAcc (1, 1,1,1)
