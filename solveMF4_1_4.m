function [X, Y, Z] = solveMF4_1_4( h, tau, L, T, v, mu, aconst )

	n = fix(L/h)+1;
    m = fix(T/tau)+1;
    
    for k=1:n
        Z(k,1) = 0;
        X(k) = (k-1)*h;
    end;

    for k=1:m
        Z(1,k) = aconst;
        Y(k) = (k-1)*tau;
    end;
	
	Z(1,1) = 0;
    
    for j=1:m-1
        for k=2:n-1
            
            Z(k,j+1)=tau*( (1/tau-2*mu/h/h)*Z(k,j)+(-v/2/h + mu/h/h)*Z(k+1,j)+ (v/2/h + mu/h/h)*Z(k-1,j));
			 
        end;
        Z(n, j+1) = (1/(1/tau+v/h))*(-Z(n,j)/tau - (v/h)*Z(n-1,j+1));

    end;
figure(1);
mesh(Y,X,Z);
title("Approximate solution");
xlabel('t');
ylabel('x');
zlabel('z');
figure(2);
XR = 0:0.01:L;
YR = 0:0.01:T;
ZR = getRealFunc(XR, YR, v, aconst);
mesh(YR, XR, ZR);
title("Accurate solution");

	function z = getRealFunc( x, t , v ,aconst )
		z = zeros(length(x), length(t));
		for i = 1:length(x)
			for j = 1:length(t)
				if (x(i)>=v*t(j))
					z(i,j) = 0;
				else
					z(i,j) = aconst;
				end;
			end;
		end;
	end;

end;

%stable: solveMF4_1_4( 0.1, 0.01, 1, 1, 3, 0.4, 1 )
%unstable: solveMF4_1_4( 0.1, 0.01, 1, 1, 3, 0.65, 1 )
