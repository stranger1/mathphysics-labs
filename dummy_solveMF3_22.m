function Y = solveMF3_22( u_0t, u_x0, aconst, l, T )
% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dt = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 22
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(t);

	result = zeros(N+1,M+1);

	for k=1:N
		result(k, 1) = u_x0(k * h);
	end;

	for k = 1:M+1
		result(1, k) = u_0t(tau * k);
	end;

	for j = 1:M	
        B = zeros(1, N); % coef near y(i-1)
		for i = 2:N-1
			B(i) = %TODO
		end;
        B(N) = %TODO

		D = zeros(1, N); % coef near y(i)
		D(1) = %TODO
		for i=2:N-1
			D(i) = %TODO 
		end;
		D(N) = %TODO

		A = zeros(1, N); % coef near y(i+1)
		A(1) = 1/(2*h);
		for i=2:N-1
			A(i) = %TODO
		end;

		f = zeros(1,N);
		f(1) = %TODO
		for i = 2:N-1			
			f(i) = %TODO
		end;
		f(N) = %TODO

		y = TDMAlg(B,D,A,f);

		for i = 1:N
			result(i, j) = y(i);
		end;
	end;


	varx = 0:l/N:l;
	vart = 0:T/M:T;
    %[xx, yy] = meshgrid(varx,vart);
    %mesh (varx(1:N+1), vart(1:N+1), result);
    plot3 (varx,vart, result);

end;
