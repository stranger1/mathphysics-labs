function z = MF4_2getG1( t, type )
	mu = MF4_2getMu(0, t, type);
    switch type
        
        case 1
            z = (6 + 5*exp(t)) * exp(t^2);
        case 2
            z = ((1 + exp(t))^2 + 2*t) * exp(t^2);
        case 3
            z = exp(-1+mu*t);
    end;


end

