function Y = solveMF3_22( u_0t, u_x0, aconst, l, T )
% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dt = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 22
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(t);

	result = zeros(N+1,M+1);

	for k=1:N+1
		result(k, 1) = u_x0(k * h);
	end;

	for j = 1:M+1
		result(1, j) = u_0t(tau * j);
	end;

	for j=1:M
		for k=1:N
			result(k+1,j+1) = (h/aconst)*(aconst/h-1/tau)*result(k,j+1) + result(k,j)/tau;
		end;
	end;

	accresult = zeros(N, M);
	for i = 1:N
		for j = 1:M

			if x(i) < aconst*t(j)
				accresult(i, j) = u_0t(t(j) - x(i)/aconst);
			else
				accresult(i, j) = u_x0(x(i) - aconst*t(j));
		end;
	end;

	varx = 0:l/N:l;
	vart = 0:T/M:T;
    %[xx, yy] = meshgrid(varx,vart);
	figure(1);
    mesh (varx(1:N+1), vart(1:N+1), result);
	figure(2);	
	mesh (x, t, accresult);
    %plot3 (varx,vart, result);

%it is always unstable
%example: solveMF3_22( @(t) t, @(x) -x, 1, 10, 10)

end;
