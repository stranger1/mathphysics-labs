function solveMF3_23( u_0t, u_x0, f_xt, a, l, T ) % f_x = 0, a - const
% seems to be the f_x shouldn't be added as parameter....or should?

% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dt = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 23
    
    %int n = Task.getTask().getN();
	%int m = Task.getTask().getM();
	%double h = Task.getTask().getH();
	%double tau = Task.getTask().getTau();
	%double a = Task.getTask().getA();
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(x);
	
	
	%double[][] result = new double[(n+1)][(m+1)];
	result = zeros(N+1, M+1);
	
	%for(int k=0; k<n+1; k++){		
	%	result[k][0] = Task.getTask().getUx0(k*h);		
	%}
	for k = 1:N+1
		result(k, 1) = u_x0(k * h);
	end;	
	
	%for(int k=0; k<m+1; k++){		
	%	result[0][k] = Task.getTask().getU0t(tau*k);		
	%}
	for k = 1:M+1
		result(1, k) = u_0t(tau * k);
	end;	
	
	%for(int j=0; j<m; j++){
	for j = 1:M		
		%double[][] matrix = new double[n][3];
		%double[] b = new double[n];
		matrix = zeros(N, 3);
		b = zeros(n);
		
		%matrix[0][1] = 1/tau;
		%matrix[0][2] = a/2/h;
		%b[0] = result[1][j]/tau + (a/2/h)*result[0][j+1];
		matrix(1, 2) = 1/tau;
		matrix(1, 3) = 1/2/h;
		b(1) = result(2, j)/tau + (a/2/h)*result(1, j+1);
		
		%matrix[n-1][0] = -a/h;
		%matrix[n-1][1] = 1/tau + a/h;
		%b[n-1] = result[n-1][j]/tau;
		matrix(n, 1) = -a/h;
		matrix(n, 2) = 1/tau + a/h;
		b(n) = result(n, j)/tau;
		
		%for(int i = 1; i<n-1; i++){			
		%	matrix[i][0] = -a/2/h;
		%	matrix[i][1] = 1/tau;
		%	matrix[i][2] = a/2/h;			
		%	b[i] = result[i][j]/tau;			
		%}		
		for i = 2:N-1
			matrix(i, 1) = -a/2/h;
			matrix(i, 2) = 1/tau;
			matrix(i, 3) = a/2/h;			
			b(i) = result(i, j)/tau;
		end;
		
		%TODO Progonka
			%Progonka pr = new Progonka();
			%b = pr.getResult(matrix, b);
		
		%TODO end;
		
		%for(int i=0; i<n; i++){
		%	result[i+1][j+1] = b[i];
		%}	
		for i = 1:N
			result(i+1, j+1) = b(i);
		end;
	end;
	
	
	accresult = zeros(N+1, M+1);
	for i = 1:N+1
		for j = 1:M+1
			if x(i) < a*t(i)
				accresult(i, j) = u_0t(t(i) - x(i)/a);
			else
				accresult(i, j) = u_x0(x(i) - a*t(i));
		end;
	end;
			
	%TODO: plot result & accresult
