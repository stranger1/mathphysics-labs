function solveMF2_2a( k_x, v_x, q_x, f_x, l, m_0, m_l )

% solve stationary convection-diffusion-reaction equation:
%   (k(x)u')' + v(x)u' - q(x)u = -f(x), 0<x<l
%   u(0)=m_0, u(l)=m_l
% using approximation of convective member
% with directional difference derivative (Samarskij''s scheme)
%
% usage:
%   solveMF2_2a( k_x, v_x, q_x, f_x, l, m_0, m_l )
    
    h = l/15;
   
    x = 0:h:l;

    N = length(x);

    function ret = vplus(x)
        ret = (x + abs(x))/2;
    endfunction

    function ret = vminus(x)
        ret = (x - abs(x))/2;
    endfunction
    
    A = zeros(1, N); % coef near y(i-1)
    for i = 2:N
        A(i) = k_x(x(i) - h/2)/(h^2) - (vminus(v_x(x(i))) * k_x(x(i) - h/2))/(k_x(x(i)) * h);
    end;
    A(N) = 0;

    C = zeros(1, N);
    C(1) = 1;
    for i = 2:N-1
        C(i) = -(k_x(x(i) - h/2) + k_x(x(i) + h/2))/(h^2) + (vminus(v_x(x(i))) * k_x(x(i) - h/2))/(k_x(x(i)) * h);
        C(i) = C(i) - (vplus(v_x(x(i))) * k_x(x(i) + h/2))/(k_x(x(i)) * h) - q_x(x(i));
    end;
    C(N) = 1;


    B = zeros(1, N); % coef near y(i+1)
    B(1) = 0;
    for i = 2:N-1    
        B(i) = k_x(x(i) + h/2)/(h^2) + (vplus(v_x(x(i))) * k_x(x(i) + h/2))/(k_x(x(i)) * h);
    end;

    %Tridiagonal matrix algorithm (Metod Progonki)
    % A(i) * y(i-1) + C(i) * y(i) + B(i) * y(i+1) = f(i)
    % y_0 = m_0
    % y_l = m_l

    f = zeros(1,N);
    f(1) = m_0;
    for i = 2:N-1
        f(i) = f_x(x(i));
    end;
    f(N) = m_l;
   
    a = zeros(1,N);
    b = zeros(1,N);
    a(1) = -B(1)/C(1);
    b(1) = f(1)/C(1); 
    for i = 1:N-1
        a(i+1) = -B(i) / (C(i) + A(i)*a(i));
        b(i+1) = (-A(i)*b(i)+f(i)) / (C(i)+A(i)*a(i));
    end;
    
    y = zeros(1,N);

    y(N) = (-A(N)*b(N)+f(N)) / (C(N)+A(N)*a(N));
    for i=N-1:-1:1
        y(i) = a(i+1)*y(i+1)+b(i+1);
    end;
    % ...
   y 
    % accurate general solution
    function ret = u(x, const1, const2)
        % example with k, v, q - const, f = 0

        k = 1;
        v = 1;
        q = 1;       

        add1 = const1 * exp((-v/(2*k) + sqrt(v^2/4*k^2 + q/k)) * x);
        add2 = const2 * exp((-v/(2*k) - sqrt(v^2/4*k^2 + q/k)) * x);
        ret = add1 + add2;
    endfunction

    % finding C1 and C2 according to initial conditions
    C2 = (m_l - m_0 * u(l, 1, 0)) / (u(l, 0, 1) - u(l, 1, 0));
    C1 = m_0 - C2;

    xx = 0:0.1:l;
    plot(x, y, xx, u(xx, C1, C2));
end
