function solveMF5_4(exampleN, tau, hx, hy, T, Lx, Ly)
% usage:
%	solveMF5_4(exampleN, tau, hx, hy, T, Lx, Ly)
%
% solve nonstationary transient heat equation:
%   du/dt = mu(x,y,t)*(d^2u/dx^2 + d^2u/dt^2) + f(x,y,t)
% using explicit scheme (4)

	nx = fix(Lx/hx) + 1;
	ny = fix(Ly/hy) + 1;
	nt = fix(T/tau) + 1;
	
% boundary conditions
	for i=1:nx
		for j=1:ny
			res(i,j,1) = MF5Acc(exampleN, (i-1)*hx, (j-1)*hy, 0);
		end;
	end;
	
	for i=1:nt
	
		for j=1:nx
			res(j,1,i) = MF5Acc(exampleN, (j-1)*hx,0,(i-1)*tau);
			res(j,ny,i) = MF5Acc(exampleN, (j-1)*hx, Ly, (i-1)*tau);
		end;
		
		for j=1:ny
			res(1,j,i) = MF5Acc(exampleN, 0, (j-1)*hy, (i-1)*tau);
			res(nx,j,i) = MF5Acc(exampleN, Lx,(j-1)*hy, (i-1)*tau);
		end;
		
	end;

% solution
	f = 0;	
	for time=1:nt-1
		
		for x=2:nx-1
			for y=2:ny-1
				mu = MF5_Mu((x-1)*hx,(y-1)*hy,(time-1)*tau,exampleN);
				res(x,y,time+1) = res(x,y,time)*(1-2*tau*mu*(1/hx/hx + 1/hy/hy)) + (tau*mu/hx/hx)*(res(x-1,y,time) + res(x+1,y,time)) + (tau*mu/hy/hy)*(res(x,y-1,time) + res(x,y+1,time)) + f;
			end;
		end;
		
	end;
	
	for i=1:nx
		X(i) = (i-1)*hx;
	end;
	
	for i=1:ny
		Y(i) = (i-1)*hy;
	end;

	for i=1:nt
		tT(i) = (i-1)*tau;
	end;

% print output
	for i=1:nx
		for j = 1:ny
			resXY(i,j) = res(i,j,1);
		end;
	end;

	for i=1:nx
		for j = 1:nt
			resXT(i,j) = res(i,1,j);
		end;
	end;
	%resXT = res(:,1,:)
	for i=1:ny
		for j = 1:nt
			resYT(i,j) = res(1,i,j);
		end;
	end;
	%resYT = res(1,:,:);
	figure(1);
	mesh(Y,X,resXY);
	title("u(x,y,0)");
	figure(2);
	mesh(tT,X,resXT);
	title("u(x,0,t)");
	figure(3);
	mesh(tT,Y,resYT);
	title("u(0,y,t)");

end

%unstable: solveMF5_4(1, 0.1,2,2,1,20,20)
%stable: solveMF5_4(1, 0.1, 0.1, 0.1, 1,1,1)
