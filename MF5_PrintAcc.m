function MF5_PrintAcc( exampleN, T, Lx, Ly )

	tT = linspace(0, T, 80);
	X = linspace(0, Lx, 80);
	Y = linspace(0, Ly, 80);

% XY projection
	for i=1:length(X)
		for j=1:length(Y)
			resXY(i,j) = MF5Acc(exampleN, 0, X(i), Y(j));
		end;
	end;

	[xx,yy] = meshgrid(X,Y);
	figure(1)
	mesh(xx,yy,resXY);
	title("u(0,y,t)");

% XT projection
	for i=1:length(X)
		for j=1:length(tT)
			resXT(j,i) = MF5Acc(exampleN, tT(j), X(i), 0);
		end;
	end;

	[xx,tt] = meshgrid(X,tT);
	figure(2)
	mesh(xx,tt,resXT);
	title("u(x,y,0)");

% YT projection
	for i=1:length(Y)
		for j=1:length(tT)
			resYT(j,i) = MF5Acc(exampleN, tT(j), 0, Y(i));
		end;
	end;

	[yy,tt] = meshgrid(Y,tT);
	figure(3)
	mesh(yy,tt,resYT);
	title("u(x,0,t)");

end
