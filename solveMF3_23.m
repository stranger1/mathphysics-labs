function solveMF3_23( u_0t, u_x0, aconst, l, T ) % f_x = 0, a - const
% seems to be the f_x shouldn't be added as parameter....or should?

% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dt = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 23
    
    %int n = Task.getTask().getN();
	%int m = Task.getTask().getM();
	%double h = Task.getTask().getH();
	%double tau = Task.getTask().getTau();
	%double a = Task.getTask().getA();
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(t);
	
	
	%double[][] result = new double[(n+1)][(m+1)];
	result = zeros(N, M);
	
	%for(int k=0; k<n+1; k++){		
	%	result[k][0] = Task.getTask().getUx0(k*h);		
	%}
	for k = 1:N
		result(k, 1) = u_x0(k * h);
	end;	
	
	%for(int k=0; k<m+1; k++){		
	%	result[0][k] = Task.getTask().getU0t(tau*k);		
	%}
	for k = 1:M
		result(1, k) = u_0t(tau * k);
	end;	
	
	%for(int j=0; j<m; j++){
	for j = 1:N-1
        B = zeros(1, N); % coef near y(i-1)
		for i = 2:N-1
			B(i) = -aconst/(2*h);
		end;
        B(N) = -aconst/h;

		D = zeros(1, N); % coef near y(i)
		D(1) = 1/tau;
		for i=2:N-1
			D(i) = 1/tau; 
		end;
		D(N) = 1/tau + aconst/h;

		A = zeros(1, N); % coef near y(i+1)
		A(1) = aconst/(2*h);
		for i=2:N-1
			A(i) = aconst/(2*h); 
		end;

		f = zeros(1,N);
		f(1) = result(1, j)/tau + (aconst/(2*h))*result(1, j+1);
		for i = 2:N-1			
			f(i) = result(i, j)/tau;
		end;
		f(N) = result(N, j)/tau;

		y = zeros(1,N);
		y = TDMAlg(B,D,A,f);
		%for(int i=0; i<n; i++){
		%	result[i+1][j+1] = b[i];
		%}	
		for i = 1:N
			result(i, j+1) = y(i);
		end;	
	end;
	
	
	accresult = zeros(N, M);
	for i = 1:N
		for j = 1:M
			if x(i) < aconst*t(j)
				accresult(i, j) = u_0t(t(j) - x(i)/aconst);
			else
				accresult(i, j) = u_x0(x(i) - aconst*t(j));
		end;
	end;
			
	%TODO: plot result & accresult

	varx = 0:h:l;
	vart = 0:tau:T;
    [xx, yy] = meshgrid(varx,vart);
	figure(1);
    mesh (varx(1:N), vart(1:M), result);
	figure(2);	
	mesh (varx(1:N), vart(1:M), accresult);    
	%plot3 (varx,vart, result);

	%test example: solveMF3_23( @(t) exp(t) - 1, @(x) sin(x), 2, 1, 1)
	%solveMF3_23( @(t) t, @(x) -x, 1, 10, 10)
end;
