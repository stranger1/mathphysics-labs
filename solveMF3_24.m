function Y = solveMF3_24( u_0t, u_x0, aconst, l, T )
% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dt = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 24
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(t);

	result = zeros(N,M);

	result(:,1) = u_x0(x);
	result(1,:) = u_0t(t);

	for j = 1:M-1
        B = zeros(1, N); % coef near y(i-1)
		for i = 2:N-1
			B(i) = -aconst/(4*h);
		end;
        B(N) = -aconst/h;

		D = zeros(1, N); % coef near y(i)
		D(1) = 1/tau;
		for i=2:N-1
			D(i) = 1/tau;
		end;
		D(N) = 1/tau + aconst/h;

		A = zeros(1, N); % coef near y(i+1)
		A(1) = aconst/(4*h);
		for i=2:N-1
			A(i) = aconst/(4*h);
		end;

		f = zeros(1,N);
		f(1) = aconst/(4*h)*result(1,j+1) + result(2,j)/tau - aconst/(4*h)*result(3,j) + aconst/(4*h)*result(1,j);
		for i = 2:N-1			
			f(i) = result(i,j)/tau - aconst/(4*h)*result(i+1,j) + aconst/(4*h)*result(i-1,j);
		end;
		f(N) = result(N,j)/tau;

		y = TDMAlg(B,D,A,f);

		for i = 1:N
			result(i, j+1) = y(i);
		end;
	end;

	varx = 0:h:l;
	vart = 0:tau:T;
    %[xx, yy] = meshgrid(varx,vart);
	mockRes = zeros(N-1,N-1);	
	for i = 2:N
		for j = 1:N-1
			mockRes(i-1,j) = result(i,j);
		end;
	end;
    mesh (varx(2:N), vart(1:N-1), mockRes);
    %mesh (varx,vart, result);

%test example: solveMF3_24( @(t) t, @(x) -x, 1, 10, 10)
% solution: t-x
end;
