function y = TDMAlg( B, D, A, f )
% D - main diagonal, B - elements below diagonal, A - elements above diagonal
% f - right vector

N = length(f);

a = zeros(1,N);
b = zeros(1,N);
a(1) = -A(1)/D(1);
b(1) = f(1)/D(1); 
for i = 1:N-1
    a(i+1) = -A(i) / (D(i) + B(i)*a(i));
    b(i+1) = (-B(i)*b(i)+f(i)) / (D(i)+B(i)*a(i));
end;

y = zeros(1,N);
y(N) = (-B(N)*b(N)+f(N)) / (D(N)+B(N)*a(N));

for i=N-1:-1:1
    y(i) = a(i+1)*y(i+1)+b(i+1);
end;    
y=y(:);
end;
