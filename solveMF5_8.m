function solveMF5_8(exampleN, tau, hx, hy, T, Lx, Ly)
% usage:
%	solveMF5_8(exampleN, tau, hx, hy, T, Lx, Ly)
%
% solve nonstationary transient heat equation:
%   du/dt = mu(x,y,t)*(d^2u/dx^2 + d^2u/dt^2) + f(x,y,t)
% using scheme (8)

	nx = fix(Lx/hx) + 1;
	ny = fix(Ly/hy) + 1;
	nt = fix(T/tau) + 1;
	
	res = zeros(nx, ny, nt);

% boundary conditions	
	for i=1:nx
		for j=1:ny
			res(i,j,1) = MF5Acc(exampleN, (i-1)*hx, (j-1)*hy, 0);
		end;
	end;
	
	for i=1:nt
	
		for j=1:nx
			res(j,1,i) = MF5Acc(exampleN, (j-1)*hx,0,(i-1)*tau);
			res(j,ny,i) = MF5Acc(exampleN, (j-1)*hx, Ly, (i-1)*tau);
		end;
		
		for j=1:ny
			res(1,j,i) = MF5Acc(exampleN, 0, (j-1)*hy, (i-1)*tau);
			res(nx,j,i) = MF5Acc(exampleN, Lx,(j-1)*hy, (i-1)*tau);
		end;
		
	end;

% solution
	for time=1:nt-1
	
		tempRes_Res = zeros(nx,ny);
		
		mu = MF5_Mu(0,0,0, exampleN);
		
		for i=2:nx-1
			gm0j1 = MF5Acc(exampleN, (i-1)*hx, 0, time*tau);
			gm0j = MF5Acc(exampleN, (i-1)*hx, 0, (time-1)*tau);
			Lambda_gm0j1 = MF5Acc(exampleN, i*hx, 0, (time)*tau) - 2*MF5Acc(exampleN, (i-1)*hx, 0, (time)*tau) + MF5Acc(exampleN, (i-2)*hx, 0, (time)*tau);
			Lambda_gm0j = MF5Acc(exampleN, i*hx, 0, (time-1)*tau) - 2*MF5Acc(exampleN, (i-1)*hx, 0, (time-1)*tau) + MF5Acc(exampleN, (i-2)*hx, 0, (time-1)*tau);
			tempRes_Res(i,1) = (gm0j1 + gm0j)/2 - tau*mu/(4*hy*hy) * ( Lambda_gm0j1 - Lambda_gm0j );

			gmNj1 = MF5Acc(exampleN, (i-1)*hx, Ly, time*tau);
			gmNj = MF5Acc(exampleN, (i-1)*hx, Ly, (time-1)*tau);
			Lambda_gmNj1 = MF5Acc(exampleN, i*hx, Ly, (time)*tau) - 2*MF5Acc(exampleN, (i-1)*hx, Ly, (time)*tau) + MF5Acc(exampleN, (i-2)*hx, Ly, (time)*tau);
			Lambda_gmNj = MF5Acc(exampleN, i*hx, Ly, (time-1)*tau) - 2*MF5Acc(exampleN, (i-1)*hx, Ly, (time-1)*tau) + MF5Acc(exampleN, (i-2)*hx, Ly, (time-1)*tau);
			tempRes_Res(i,ny) = (gmNj1 + gmNj)/2 - tau*mu/(4*hy*hy) * ( Lambda_gmNj1 - Lambda_gmNj );
		end;
		
		for i=1:ny
			g0nj1 = MF5Acc(exampleN, 0, (i-1)*hy, time*tau);			
			g0nj = MF5Acc(exampleN, 0, (i-1)*hy, (time-1)*tau);
			Lambda_g0nj1 = MF5Acc(exampleN, 0, i*hy, (time)*tau) - 2*MF5Acc(exampleN, 0, (i-1)*hy, (time)*tau) + MF5Acc(exampleN, 0, (i-2)*hy, (time)*tau);
			Lambda_g0nj = MF5Acc(exampleN, 0, i*hy, (time-1)*tau) - 2*MF5Acc(exampleN, 0, (i-1)*hy, (time-1)*tau) + MF5Acc(exampleN, 0, (i-2)*hy, (time-1)*tau);
			tempRes_Res(1,i) = (g0nj1 + g0nj)/2 - tau*mu/(4*hx*hx) * ( Lambda_g0nj1 - Lambda_g0nj );

			gNnj1 = MF5Acc(exampleN, Lx, (i-1)*hy, time*tau);			
			gNnj = MF5Acc(exampleN, Lx, (i-1)*hy, (time-1)*tau);
			Lambda_gNnj1 = MF5Acc(exampleN, Lx, i*hy, (time)*tau) - 2*MF5Acc(exampleN, Lx, (i-1)*hy, (time)*tau) + MF5Acc(exampleN, Lx, (i-2)*hy, (time)*tau);
			Lambda_gNnj = MF5Acc(exampleN, Lx, i*hy, (time-1)*tau) - 2*MF5Acc(exampleN, Lx, (i-1)*hy, (time-1)*tau) + MF5Acc(exampleN, Lx, (i-2)*hy, (time-1)*tau);
			tempRes_Res(1,i) = (gNnj1 + gNnj)/2 - tau*mu/(4*hx*hx) * ( Lambda_gNnj1 - Lambda_gNnj );

			%tempRes_Res(1,i) = MF5Acc(exampleN, 0, (i-1)*hy, (time - 1/2)*tau);
			%tempRes_Res(nx,i) = MF5Acc(exampleN, Lx, (i-1)*hy, (time - 1/2)*tau);
		end;
		
		for j=2:ny-1
		
			tempMatrix = zeros(nx-2,3);
			tempB = zeros(nx-2,1);
			
			tempB(1) = res(2,j-1, time)*mu/hy/hy + res(2,j,time)*(2/tau - 2*mu/hy/hy) + res(2, j+1, time)*mu/hy/hy + MF5Acc(exampleN, 0, (j-1)*hy, (time-1/2)*tau)*mu/hx/hx;			
			
			
			for i=2:nx-1
			
				tempMatrix(i-1,1) = -mu/hx/hx;
				tempMatrix(i-1,2) = 2/tau + 2*mu/hx/hx;
				tempMatrix(i-1,3) = -mu/hx/hx;
			
				tempB(i-1) = res(i,j-1, time)*mu/hy/hy + res(i,j,time)*(2/tau - 2*mu/hy/hy) + res(i, j+1, time)*mu/hy/hy;
			
			end;
			
			tempMatrix(1,1) = 0;
			tempMatrix(nx-2,3) = 0;
			
			tempB(1) = tempB(1) +  + MF5Acc(exampleN, 0, (j-1)*hy, (time-1/2)*tau)*mu/hx/hx;
			tempB(nx-2) = tempB(nx-2) + MF5Acc(exampleN, Lx, (j-1)*hy, (time-1/2)*tau)*mu/hx/hx;
			
			tempRes = progonka(tempMatrix,tempB);
			
			tempn = length(tempRes);

			for it=1:tempn
				tempRes_Res(it+1,j) = tempRes(it);
			end;

		end;

		
		for j=2:nx-1
		
			temp2Matrix = zeros(ny-2,3);
			temp2B = zeros(ny-2,1);

			for i=2:ny-1
			
				temp2Matrix(i-1,1) = -mu/hy/hy;
				temp2Matrix(i-1,2) = 2/tau + 2*mu/hy/hy;
				temp2Matrix(i-1,3) = -mu/hy/hy;
			
				temp2B(i-1) = tempRes_Res(j-1,i)*mu/hy/hy + tempRes_Res(j,i)*(2/tau - 2*mu/hy/hy) + tempRes_Res(j+1, i)*mu/hx/hx;
			
			end;
			
			temp2Matrix(1,1) = 0;
			temp2Matrix(ny-2,3) = 0;
			
			temp2B(1) = temp2B(1) + MF5Acc(exampleN, (j-1)*hx, 0, (time)*tau)*mu/hy/hy;
			temp2B(ny-2) = temp2B(ny-2) + MF5Acc(exampleN, (j-1)*hx, Ly, (time)*tau)*mu/hy/hy;
			
			temp2Res = progonka(temp2Matrix,temp2B);
				
			tempn = length(temp2Res);
				
			for it=1:tempn
				res(j,it+1,time+1) = temp2Res(it);
			end;
			
		end;
		
		
	end;
	
	for i=1:nx
		X(i) = (i-1)*hx;
	end;
	
	for i=1:ny
		Y(i) = (i-1)*hy;
	end;

	for i=1:nt
		tT(i) = (i-1)*tau;
	end;

% print output
	for i=1:nx
		for j = 1:ny
			resXY(i,j) = res(i,j,1);
		end;
	end;
	%resXY = res(:,:,1);
	for i=1:nx
		for j = 1:nt
			resXT(i,j) = res(i,1,j);
		end;
	end;
	%resXT = res(:,1,:)
	for i=1:ny
		for j = 1:nt
			resYT(i,j) = res(1,i,j);
		end;
	end;
	%resYT = res(1,:,:);
	figure(1);
	mesh(Y,X,resXY);
	title("u(x,y,0)");
	figure(2);
	mesh(tT,X,resXT);
	title("u(x,0,t)");
	figure(3);
	mesh(tT,Y,resYT);
	title("u(0,y,t)");

end

%example: solveMF5_8(1, 0.01, 0.2, 0.1, 1,1,1)
