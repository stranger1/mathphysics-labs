function Y = solveMF3_6( u_0t, u_x0, aconst, l, T )
% solve non-stationary convective transfer equation
%   du/dt + a(x,t)*du/dx = f(x, t)
%   u(x, 0) = u_x0, u(0, y) = u_0t, f(x,t) = 0; a = const
% using scheme 6
	h = l/15;
	x = 0:h:l;
	N = length(x);
	
	tau = T/15;
	t = 0:tau:T;
	M = length(t);

	result = zeros(N,M);

	result(:,1) = u_x0(x);
	result(1,:) = u_0t(t);

	for j=1:M-1
   	    for k=1:N-1
        	b = aconst*tau/h;
        	result(k+1,j+1)= (1-b)*result(k+1,j)+ b*result(k,j);
        end;
	end;
	
	accresult = zeros(N, M);
	for i = 1:N
		for j = 1:M

			if x(i) < aconst*t(j)
				accresult(i, j) = u_0t(t(j) - x(i)/aconst);
			else
				accresult(i, j) = u_x0(x(i) - aconst*t(j));
		end;
	end;

	varx = 0:h:l;
	vart = 0:tau:T;
    %[xx, yy] = meshgrid(varx,vart);
    figure(1);
    mesh (varx, vart, result);
    
    figure(2)
    mesh (varx, vart, accresult);
    %plot3 (varx,vart, result);

%example test: solveMF3_6( @(t) t, @(x) -x, 1, 0.5, 0.1)
%if aconst*tau/h > 1 - scheme is unstable

end;
